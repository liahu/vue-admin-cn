import pkg from '../package.json'

export const ROUTES_LINKS = [
  { title: '首页', path: '/' },
  { title: 'Admin', path: '/admin' },
  { title: 'UI组件库', path: '/ui' },
  { title: 'Hooks', path: '/hooks' },
  { title: 'CLI', path: '/cli' },
  { title: 'UniApp', path: '/uniapp' },
  { title: '关于我们', path: '/help/about/', isBlank: false },
  // { title: '帮助中心', path: '/docs', isBlank: true },
]

interface ICodeMessage {
  [propName: number]: string
}

export const CodeMessage: ICodeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  405: '请求方法不被允许。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
}

// 基础配置
export default {
  name: pkg.name,
  version: pkg.version,
  title: 'vue-admin',
  author: 'Admin',
  description: 'Vue前端交流群 - 364912432',
  keywords: 'vue2,vue3,Vue前端,Vue前端管理模板,vue前端UI组件库,vue前端hooks工具集,前端技术开发,javascript技术',
}

// 版本
export const APP_VERSION = pkg.version
// 空白页面
export const BlankList = ['/example/blank']
// 项目名称
export const ProjectName = 'Vue-Admin'
export const ProjectTitle = ProjectName.replace(/\s+/g, '')

// 存储key
export const StoreKey = `${ProjectTitle}-${APP_VERSION}`
export const LoginPath = '/login'
// 公告板
export const HomePath = '/basic/dashboard'
// 开源地址
export const Github = 'https://gitee.com/jsfront/vue-admin-cn'
export const Gitee = 'https://gitee.com/jsfront/vue-admin-cn'
// 分页配置
export const BasePagination = { page: 1, limit: 8 }
// 协议
export const Protocol = 'http'
// 域名
export const Domain = 'vue-admin.cn'
// APPID
export const App_Id = 1
// 标签颜色
export const TagColorList = ['red', 'orangered', 'orange', 'deeppink', 'lightcoral', 'green', 'cornflowerblue', 'blue', 'darkslategrey', 'purple', 'pinkpurple', 'magenta', 'gray', 'darkolivegreen', 'lightseagreen']
// 百度统计
export const baiduGA = 'https://hm.baidu.com/hm.js?908a80c9d6088b0142172792b48ec120'
