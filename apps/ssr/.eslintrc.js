module.exports = {
  plugins: ['prettier'],
  extends: ['@nuxtjs/eslint-config-typescript', 'plugin:import/typescript', 'prettier', 'plugin:prettier/recommended'],
  rules: {
    'vue/multi-word-component-names': 0,
    'prettier/prettier': 'error',
    'no-console': 'off',
    'vue/attribute-hyphenation': [1, 'always'], // 组件上属性名使用带连字符
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always',
        },
        svg: 'always',
        math: 'always',
      },
    ],
    'camelcase': 0,
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'vue/no-v-model-argument': 'off',
  },
}
