// 获取文章列表
export const useArticleData = (params?: any) => useHttpGet('ArticleData', 'article', { lazy: true, params })
// 获取文章详情
export const useArticleDetail = (params?: any) => useHttpGet('ArticleDetail', `article/${params.id}`, { lazy: true, params })
// 新增文件
export const fetchArticleAdd = (body?: any) => useHttpPost('FetchArticle', 'article/add', { body })
// 获取后台语言列表
export const useBackendData = (params?: any) => useHttpGet('BackendData', 'backend', { lazy: true, params })
// 获取标签列表
export const useTagsData = (params?: any) => useHttpGet('TagsData', 'tags', { lazy: true, params })
