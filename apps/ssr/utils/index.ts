import { ElNotification } from 'element-plus'

export const isDev = process.env.NODE_ENV === 'development'

// 格式化参数
export function setParams(params: any) {
  const searchParams = new URLSearchParams()

  Object.keys(params).forEach((key: any) => {
    searchParams.set(key, params[key])
  })

  return searchParams.toString()
}

// 输出通知
export const notice = (message: string, type: any = 'success', title = '友情提醒') => {
  ElNotification({
    title,
    message,
    type,
  })
}

// 浏览器环境
export const isBrowser = (): boolean => {
  return typeof window !== 'undefined'
}

/**
 * 分享到
 * @param options
 * url: 项目访问地址
 * title: 项目名称
 * pics: 项目的封面图片地址
 * summary: 摘要
 * desc: 描述
 * appkey: 新浪微博分享appkey
 */
export const shareTo = (options: any) => {
  const { url, title, pics, type, summary, desc, appkey } = options

  // qq空间接口的传参
  if (type === 'qzone') {
    window.open(`https://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=${url}?sharesource=qzone&title=${title}&pics=${pics}&summary=${summary}`)
  }
  // 新浪微博接口的传参
  if (type === 'weibo') {
    window.open(`http://service.weibo.com/share/share.php?url=${url}?sharesource=weibo&title=${title}&pic=${pics}&appkey=${appkey}`)
  }
  // qq好友接口的传参
  if (type === 'qq') {
    window.open(`http://connect.qq.com/widget/shareqq/index.html?url=${url}?sharesource=qzone&title=${title}&pics=${pics}&summary=${summary}&desc=${desc}`)
  }
}

// 滚动到
export const scrollTo = (id: string, contentId: string) => {
  const el = document.querySelector(id) as HTMLElement
  const content = document.querySelector(contentId) as HTMLElement

  el &&
    el.scrollIntoView({
      behavior: 'smooth', // 定义动画过渡效果， "auto"或 "smooth" 之一。默认为 "auto"
      block: 'center', // 定义垂直方向的对齐， "start", "center", "end", 或 "nearest"之一。默认为 "start"
      inline: 'nearest', // 定义水平方向的对齐， "start", "center", "end", 或 "nearest"之一。默认为 "nearest"
    })
  content.focus()
}

// 获取随机标签颜色
export const getTagType = (index: number, step: number = 4) => {
  return ['success', 'info', 'warning', 'danger'][index % step]
}
