// https://v3.nuxtjs.org/api/configuration/nuxt.config
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import Components from 'unplugin-vue-components/vite'
import AppConfig, { ProjectName, baiduGA } from './config'

const { keywords, description } = AppConfig

export default defineNuxtConfig({
  ssr: true,
  app: {
    head: {
      title: ProjectName,
      meta: [
        { name: 'viewport', content: 'width=device-width, user-scalable=no, initial-scale=1.0, shrink-to-fit=no, viewport-fit=cover' },
        { name: 'keywords', content: keywords },
        { name: 'description', content: description },
      ],
      link: [{ rel: 'icon', type: 'image/x-icon', href: '/logo.png' }],
      script: [{ src: baiduGA }],
    },
  },

  modules: ['@unocss/nuxt', '@nuxtjs/color-mode', 'nuxt-icon', 'nuxt-lodash'],

  plugins: ['@/plugins/element-plus', '@/plugins/clipboard', '@/plugins/vueInject.js', '@/plugins/baidu-ga.js'],

  // 全局设置的 CSS 文件
  css: ['@/assets/css/index.scss'],

  vite: {
    plugins: [
      // ...
      Components({
        resolvers: [IconsResolver()],
      }),
      Icons({ autoInstall: true }),
    ],
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/css/variable.scss";@import "@/assets/css/mixin.scss";',
        },
      },
    },
    ssr: {
      noExternal: ['moment'],
    },
  },

  imports: {
    dirs: ['api'],
  },
})
