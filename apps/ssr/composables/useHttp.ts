import { ElMessage } from 'element-plus'
import { App_Id } from '~~/config'

const apiUrl = import.meta.env.VITE_API_URL

export const fetchConfig = {
  baseURL: `${apiUrl}api/`,
  headers: {
    'appid': 'bd9d01ecc75dbbaaefce',
    'Content-Type': 'application/json',
  },
}

// 请求体封装
function useGetFetchOptions(options: any = {}) {
  const source = { appId: App_Id }

  options.baseURL = options.baseURL ?? fetchConfig.baseURL
  options.params && Object.assign(options.params, source)
  options.body && Object.assign(options.body, source)

  return options
}

// http请求封装
export async function useHttp(key: string, url: string, options: any = {}) {
  options = useGetFetchOptions(options)
  options.key = key

  const data = ref(null)
  const error = ref(null)

  return await $fetch(url, options)
    .then((res: any) => {
      data.value = res.data
      return { data, error }
    })
    .catch((err) => {
      const msg = err?.data?.data
      if (process.client) {
        ElMessage.error(msg || '服务端错误')
      }
      error.value = msg

      return { data, error }
    })
}

// GET请求
export function useHttpGet(key: string, url: string, options: any = {}) {
  options.method = 'GET'
  return useHttp(key, url, options)
}

// POST请求
export function useHttpPost(key: string, url: string, options: any = {}) {
  options.method = 'POST'
  return useHttp(key, url, options)
}
