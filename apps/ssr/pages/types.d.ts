declare interface INavList {
  value: number
  label: string
}

declare interface ITopList extends INavList {
  title: string
  class: string
}
