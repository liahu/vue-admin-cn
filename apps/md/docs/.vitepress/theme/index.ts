import Theme from 'vitepress/theme'
import { h } from 'vue'
// import HeroBefore from './components/HeroBefore.vue'
import FreeStyle from './components/FreeStyle.vue'
import './scss/global.scss'

export default {
  ...Theme,
  Layout() {
    return h(Theme.Layout, null, {
      // 'home-hero-before': () => h(HeroBefore)
    })
  },
  enhanceApp({ app }: any) {
    app.component('FreeStyle', FreeStyle)
  }
}