---
date: 2020-11-17
title: 初始化
describe: 初始化
---

# 初始化


- node 版本管理工具

    - [nvm](https://github.com/nvm-sh/nvm)
    - [nvm-windows](https://github.com/coreybutler/nvm-windows)

- 备选工具

    - [node 版本管理工具 - fnm](https://github.com/Schniz/fnm)
    - [npm 源管理工具 - nrm](https://github.com/Pana/nrm)

- 教程

    - [使用 nvm 管理不同版本的 node 与 npm](https://www.runoob.com/w3cnote/nvm-manager-node-versions.html)
    - [npm 源管理器 nrm 使用教程](https://segmentfault.com/a/1190000017419993)


```
// 切换环境
nvm install 16.0.0
nvm use 16.0.0

// 安装依赖
npm install

// 启动项目
npm start

// 清除 node_modules
npm run clean

// 全局安装 rimraf 之后方可使用
npm i rimraf -g

// 清除 node_modules 重新安装依赖
// 等同于 npm run clean && npm install
npm run reinstall

```